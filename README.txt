Installation and Demo Instructions

NOTE: If you want to run in an IDE, go to gui / Main and run it's main function.
Attached is a Surly_FemDaryl.jar file. This is an executable jar that will launch the program.

When the program is launched you can type in a query, or select a .txt file with the 'Insert File' button that will insert all of its contents.

Next, you can select the 'Run' button to run the query.

When Surly comes across a print statement, it will launch another window showing the relations or catalog.

SHORTCUTS:
CTRL + ENTER: Open File Window
SHIFT + ENTER: Run Query
ESC: Close window