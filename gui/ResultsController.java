import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;

import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;

public class ResultsController implements Initializable {

    @FXML private Label relationName;
    @FXML private Button prevButton;
    @FXML private Button nextButton;
    @FXML private TableView relationTableView;
    private ObservableList<ObservableList> data;
    int relationCounter = 0;

    public ResultsController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fillTableData();

        prevButton.setOnAction(e -> {
            getPrevRelation();
            fillTableData();
        });

        nextButton.setOnAction(e -> {
            getNextRelation();
            fillTableData();
        });
    }

    private void getNextRelation() {
        if(relationCounter + 1 >= SurlyDatabase.relationIndexes.size()) {
            relationCounter = 0;
        } else {
            relationCounter++;
        }
    }

    private void getPrevRelation() {
        if(relationCounter - 1 < 0) {
            relationCounter = SurlyDatabase.relationIndexes.size() - 1;
        } else {
            relationCounter--;
        }
    }

    private void fillTableData() {
        relationTableView.getItems().clear();
        relationTableView.getColumns().clear();
        data = buildData(SurlyDatabase.relationIndexes.get(relationCounter));
        findRelationName(SurlyDatabase.relationIndexes.get(relationCounter));
        sendRelationToGUI(SurlyDatabase.relationIndexes.get(relationCounter));
    }


    private void sendRelationToGUI(int relationIndex) {
        for (int i = 0; i < SurlyDatabase.relations.get(relationIndex).getSchema().attributes.size(); i++) {
            int j = i;
            TableColumn newColumn = new TableColumn<>(SurlyDatabase.relations.get(relationIndex).getSchema().attributes.get(i).getName());
            newColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList,String>,ObservableValue<String>>(){
                public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                    return new SimpleStringProperty(param.getValue().get(j).toString());
                }
            });
            relationTableView.getColumns().add(newColumn);
        }
        relationTableView.setItems(data);
    }

    private void findRelationName(int relationIndex) {
        relationName.setText(SurlyDatabase.relations.get(relationIndex).getName());
        relationName.setAlignment(Pos.CENTER);
    }

    private ObservableList<ObservableList> buildData(int relationIndex) {
        ObservableList<ObservableList> data = FXCollections.observableArrayList();

        LinkedList<Tuple> attributes = SurlyDatabase.relations.get(relationIndex).tuples;
        for(Tuple rows : attributes) {
            ObservableList<String> newRow = FXCollections.observableArrayList();
            for (Field field : rows.attributes) {
                newRow.add(field.getValue());
            }
            data.add(newRow);
        }
        return data;
    }

    public void handleKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ESCAPE) {
            ((Node)(keyEvent.getSource())).getScene().getWindow().hide();
        } else if(keyEvent.getCode() == KeyCode.RIGHT) {
            getNextRelation();
            fillTableData();

        } else if(keyEvent.getCode() == KeyCode.LEFT) {
            getPrevRelation();
            fillTableData();
        }
    }
}
