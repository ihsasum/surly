import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Button btnFileOpen;

    @FXML
    private Label lblFileName;

    @FXML
    private TextArea txtQuery;

    @FXML
    private TextArea txtOutput;

    KeyCombination shiftEnter = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.SHIFT_DOWN);
    KeyCombination controlEnter = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN);

    public Controller() {}

    public void appendText(String str) {
        Platform.runLater(() -> txtOutput.appendText(str));
    }

    @FXML
    public void openFile() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT file (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(btnFileOpen.getScene().getWindow());
        if(file != null) {
            txtQuery.clear();
            lblFileName.setText("..." + file.toString());
            StringBuilder printContents = new StringBuilder("");
            ArrayList<String> fileContents = InsertFile.intoQuery(file.toString());
            for(String line : fileContents) {
                printContents.append(line + "\n");
            }
            txtQuery.setText(printContents.toString());
        }
        txtQuery.requestFocus();
    }

    public void runQuery() {
        txtOutput.clear();
        OutputStream out = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                appendText(String.valueOf((char)b));
            }
        };
        System.setOut(new PrintStream(out, true));

        String query = txtQuery.getText();
        LexicalAnalyzer queryAnalyzer = new LexicalAnalyzer();
        queryAnalyzer.readQuery(query);
        checkForPrintStatement();
        checkForCatalogStatement();
    }

    private void checkForCatalogStatement() {
        if(SurlyDatabase.hasCatalogStatement) {
            SurlyDatabase.hasCatalogStatement = false;
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/styles/CatalogGUI.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.sizeToScene();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Catalog");
                stage.setScene(new Scene(root, 800, 600));
                stage.show();
                txtQuery.requestFocus();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    private void checkForPrintStatement() {
        if(SurlyDatabase.hasPrintStatement && !SurlyDatabase.hasCatalogStatement) {
            try {
                SurlyDatabase.hasPrintStatement = false;
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/styles/ViewRelationsGUI.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.sizeToScene();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Results");
                stage.setScene(new Scene(root, 800, 600));
                stage.show();
                txtQuery.requestFocus();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtOutput.setEditable(false);
    }

    public void handleKeyPressed(KeyEvent keyEvent) {
        if(shiftEnter.match(keyEvent)) {
            runQuery();
        } else if(controlEnter.match(keyEvent)) {
            openFile();
        } else if(keyEvent.getCode() == KeyCode.ESCAPE) {
            ((Node)(keyEvent.getSource())).getScene().getWindow().hide();
        }
    }

}
