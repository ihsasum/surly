import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CatalogController implements Initializable {

    @FXML private Label catalogLabel;
    @FXML private TreeView catalogTreeView;

    public CatalogController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        catalogLabel.setAlignment(Pos.CENTER);
        TreeItem<String> root = new TreeItem<String>("Root Node");
        root.setExpanded(true);
        catalogTreeView.setShowRoot(false);
        for(int i = 0; i < SurlyDatabase.relations.size(); i++) {
            TreeItem<String> relation = new TreeItem<>(SurlyDatabase.relations.get(i).getName());
            relation.setExpanded(false);
            root.getChildren().add(relation);
            for (Field field : SurlyDatabase.relations.get(i).getSchema().attributes) {
                TreeItem<String> attribute = new TreeItem<>(field.getName() + " ("  + field.getType() + " " + field.getLength() + ")");
                relation.getChildren().add(attribute);
            }
        }
        catalogTreeView.setRoot(root);
    }

    @FXML
    public void handleKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ESCAPE) {
            ((Node)(keyEvent.getSource())).getScene().getWindow().hide();
        }
    }
}
