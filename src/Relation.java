import java.util.LinkedList;

public class Relation {
    LinkedList<Tuple> tuples = new LinkedList<>();
    private String name;
    private Tuple schema;

    public Tuple getSchema() {
        return schema;
    }
    public void setSchema(Tuple schema) {
        this.schema = schema;
    }

    public LinkedList<Tuple> getTuples() {
        return tuples;
    }
    public void setTuples(LinkedList<Tuple> tuples) {
        this.tuples = tuples;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Relation(String name) {
        this.name = name;
    }

    public void print() {
        for(Tuple tuple : tuples) {
            tuple.print();
            System.out.println();
        }
    }

    public void printSchema() {
        for(Field field : schema.attributes) {
            System.out.print("| ");
            System.out.print(field.getName() + " ");
        }
        System.out.print(" |");
        System.out.println();
    }
}
