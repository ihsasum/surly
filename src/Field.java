public class Field extends Attribute {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Field(){}
    public Field(String name, String type, String length) {
        super(name, type, length);
    }


}
