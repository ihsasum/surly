import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class InsertFile {
    public static ArrayList<String> intoQuery(String filename) {
        ArrayList<String> filecontents = new ArrayList<>();
        try {
            Path path = Paths.get(filename);
            List<String> file =  Files.readAllLines(path);
            for(String line : file) {
                filecontents.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filecontents;
    }
}
