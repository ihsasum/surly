import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class LexicalUtils {
    public static void handleRelation(String queryLine) {
        String[] relationLine = queryLine.split("[(]");
        Relation newRelation = new Relation(relationLine[0].substring(relationLine[0].indexOf("N") + 1).trim());
        Tuple newTuple = findRelationAttributes(relationLine[1].trim());
        newRelation.setSchema(newTuple);
        SurlyDatabase.relations.add(newRelation);
        System.out.println("Successfully added relation " + newRelation.getName());
    }

    public static void handleInsert(String queryLine) {
        String[] insertLine = queryLine.split(" ");
        String insertIntoRelation = insertLine[1];
        Tuple insertAttributes = findInsertAttributes(queryLine);

        for(int i = 0; i < SurlyDatabase.relations.size(); i++) {
            if(SurlyDatabase.relations.get(i).getName().toUpperCase().equals(insertIntoRelation.toUpperCase())) {
                SurlyDatabase.printRelationName = SurlyDatabase.relations.get(i).getName();
                SurlyDatabase.relations.get(i).tuples.add(insertAttributes);
                //System.out.println("Successfully inserted into relation " + SurlyDatabase.relations.get(i).getName());
                SurlyDatabase.insertCounter++;
            }
        }
    }

    public static void handlePrint(String queryLine) {
        String[] printLine = queryLine.trim().split(" ");
        if (printLine.length == 2) {
            if(printLine[1].contains(";")) {
                printLine[1] = printLine[1].replace(";", "");
            }
            if(printLine[1].toUpperCase().equals("CATALOG")) { SurlyDatabase.hasCatalogStatement = true; }
            getRelationIndexes(printLine[1].trim());
        } else {
            ArrayList<String> printRelationList = SurlyPrinter.findRelationsToPrint(queryLine);
            getRelationIndexes(printRelationList);
        }
    }

    public static void handleDestroy(String queryLine) {
        String[] destroyLine = queryLine.trim().split(" ");
        if(destroyLine[1].contains(";")) {
            destroyLine[1] = destroyLine[1].replace(";", "");
        }
        for(int i = 0; i < SurlyDatabase.relations.size(); i++) {
            if(SurlyDatabase.relations.get(i).getName().equals(destroyLine[1].toUpperCase().trim())) {
                System.out.println("Successfull removed relation " + SurlyDatabase.relations.get(i).getName());
                SurlyDatabase.relations.remove(i);
                break;
            }
        }
    }

    public static void handleDelete(String queryLine) {
        String[] deleteLine = queryLine.trim().split(" ");
        if(deleteLine[1].contains(";")) {
            deleteLine[1] = deleteLine[1].replace(";", "");
        }
        for(int i = 0; i < SurlyDatabase.relations.size(); i++) {
            if(SurlyDatabase.relations.get(i).getName().equals(deleteLine[1].toUpperCase().trim())) {
                SurlyDatabase.relations.get(i).tuples = new LinkedList<Tuple>();
                System.out.println("Successfully removed all rows from " + SurlyDatabase.relations.get(i).getName());
                break;
            }
        }
    }

    public static void handleProject(String queryLine) {
        String[] projectLine = queryLine.split("=");
        Relation newRelation = new Relation(projectLine[0].trim());
        String[] searchRelation = projectLine[1].toUpperCase().trim().split("FROM");
        String relationAttributes = searchRelation[0].toUpperCase().replace("PROJECT", "").trim();
        String[] searchAttributes = relationAttributes.split(",");
        ArrayList<String> attributes = new ArrayList<>();
        ArrayList<Integer> attributeIndexes = new ArrayList<>();
        for(String attr : searchAttributes){
            attributes.add(attr.trim());
        }
        // remove attributes
        for(Relation relation : SurlyDatabase.relations) {
            if(relation.getName().equals(searchRelation[1].replace(";", "").trim())) {
                Tuple newSchema = new Tuple();
                for (int i = 0; i < relation.getSchema().attributes.size(); i++) {
                    for(String attr : attributes) {
                        if(attr.equals(relation.getSchema().attributes.get(i).getName())) {
                            newSchema.attributes.add(relation.getSchema().attributes.get(i));
                            attributeIndexes.add(i);
                        }
                    }
                    if(!attributes.contains(relation.getSchema().attributes.get(i).getName()))  {
                        for(Tuple deleteAttr : newRelation.tuples) {
                            deleteAttr.attributes.remove(relation.getSchema().attributes.get(i));
                        }
                    }
                }
                newRelation.setSchema(newSchema);
                LinkedList<Tuple> newTuples = new LinkedList<>();
                for(Tuple tuple : relation.getTuples()) {
                    Tuple newTuple = new Tuple();
                    for (int i = 0; i < tuple.attributes.size(); i++) {
                        for(Integer index : attributeIndexes) {
                            if(String.valueOf(index).equals(String.valueOf(i))) {
                                newTuple.attributes.add(tuple.attributes.get(i));
                            }
                        }
                    }
                    newTuples.add(newTuple);
                }
                newRelation.setTuples(newTuples);
                System.out.println("Successfully moved data from " + relation.getName() + " to " + newRelation.getName());
            }
        }
        SurlyDatabase.relations.add(newRelation);
    }

    public static Tuple findRelationAttributes(String line) {
        String[] attributeLine = line.substring(0, line.indexOf(")")).split(",");
        Tuple tuple = new Tuple();

        for(String attribute : attributeLine) {
            String[] seperateAttributes = attribute.trim().split(" ");
            String name = seperateAttributes[0].trim();
            String type = seperateAttributes[1].trim();
            String length = seperateAttributes[2].trim();
            Field newField = new Field(name, type, length);
            tuple.attributes.add(newField);
        }
        return tuple;
    }

    public static Tuple findInsertAttributes(String line) {
        String[] lineList = line.split(" ");
        Boolean foundCharString = false;
        String attribute = "";
        ArrayList<String> newTupleAttributes = new ArrayList<>();
        LinkedList<Field> fields = new LinkedList<>();
        Tuple newTuple = new Tuple();

        for(int i = 2; i < lineList.length; i++) {
            Field newField = new Field();

            if(lineList[i].trim().startsWith("'") && (lineList[i].trim().endsWith("'") || lineList[i].trim().endsWith(";")))  {
                attribute = lineList[i].replace("'","").replace(";", "");
                newTupleAttributes.add(attribute.trim());
                newField.setValue(attribute.trim());
                attribute = "";
                fields.add(newField);

            } else if(lineList[i].trim().startsWith("'")) {
                foundCharString = true;
                attribute = lineList[i].replace("'", "").trim();

            } else if (lineList[i].trim().endsWith("'")) {
                foundCharString = false;
                attribute += " " + lineList[i].replace("'", "").trim();
                newTupleAttributes.add(attribute.trim());
                newField.setValue(attribute.trim());
                fields.add(newField);
                attribute = "";

            } else if (foundCharString) {
                attribute += " " + lineList[i].trim();

            } else if (lineList[i].endsWith(";")) {
                newTupleAttributes.add(lineList[i].replace(";", "").trim());
                newField.setValue(lineList[i].replace(";", "").trim());
                fields.add(newField);

            } else {
                newTupleAttributes.add(lineList[i].trim());
                newField.setValue(lineList[i].trim());
                fields.add(newField);
            }
        }

        for(Field field : fields) {
            newTuple.attributes.add(field);
        }
        return newTuple;
    }

    public static void getRelationIndexes(String relationName) {
        ArrayList<Integer> relationIndexes = new ArrayList<>();
        for(int i = 0; i < SurlyDatabase.relations.size(); i++) {
            if(SurlyDatabase.relations.get(i).getName().trim().toUpperCase().equals(relationName.trim().toUpperCase())) {
                relationIndexes.add(i);
            }
        }
        SurlyDatabase.relationIndexes = relationIndexes;
    }

    public static void getRelationIndexes(ArrayList<String> relationNames) {
        ArrayList<Integer> relationIndexes = new ArrayList<>();
        for(String relation : relationNames) {
            for(int i = 0; i < SurlyDatabase.relations.size(); i++) {
                if(SurlyDatabase.relations.get(i).getName().toUpperCase().trim().equals(relation.trim().toUpperCase())) {
                    relationIndexes.add(i);
                }
            }
        }
        SurlyDatabase.relationIndexes = relationIndexes;
    }
}
