import java.util.LinkedList;

public class Tuple {
    public LinkedList<Field> attributes = new LinkedList<>();

    public void print() {
        for(Field field : attributes) {
            System.out.print("| ");
            System.out.print(field.getValue() + " ");
        }
        System.out.print(" |");
    }

}
