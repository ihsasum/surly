public class Attribute {
    private String name;
    private String type;
    private String length;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getLength() {
        return length;
    }

    public Attribute(String name, String type, String length) {
        this.name = name;
        this.type = type;
        this.length = length;
    }

    public Attribute() {}

}
