import java.util.*;

public class LexicalAnalyzer {

    public static void readQuery(String query) {
        String queryArray[] = query.split("\n");
        ArrayList<String> queryList = new ArrayList<>(Arrays.asList(queryArray));

        for(String queryLine : queryList) {
            if(queryLine.trim().toUpperCase().startsWith("RELATION")) {
                LexicalUtils.handleRelation(queryLine);

            } else if (queryLine.trim().toUpperCase().startsWith("INSERT")) {
                LexicalUtils.handleInsert(queryLine);

            } else if (queryLine.trim().toUpperCase().startsWith("PRINT")) {
                LexicalUtils.handlePrint(queryLine);
                SurlyDatabase.hasPrintStatement = true;

            } else if (queryLine.trim().toUpperCase().startsWith("DESTROY")) {
                LexicalUtils.handleDestroy(queryLine);

            } else if (queryLine.trim().toUpperCase().startsWith("DELETE")) {
                LexicalUtils.handleDelete(queryLine);

            } else if (queryLine.trim().toUpperCase().contains("=PROJECT") || queryLine.trim().toUpperCase().contains("= PROJECT")) {
                LexicalUtils.handleProject(queryLine);

            } else {
                System.out.println("This is not a valid query");

            }
        }
    }
}
