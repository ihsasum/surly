import java.util.ArrayList;

public class SurlyPrinter {
    //TODO: GET RID OF THESE PRINT STATEMENTS, MAKE TERMINAL OUTPUTS SHOWING ACTIVITY SUCH AS INSERTS, NEW RELATIONS BEING MADE

    public static ArrayList<String> findRelationsToPrint(String line) {
        String relation = "";
        String[] printLine = line.split(" ");
        ArrayList<String> relationNames = new ArrayList<>();
        for(int i = 1; i < printLine.length; i++) {
            if(printLine[i].contains(",")) {
                relation = printLine[i].replace(",", "");
                if(checkForCatalogStatement(relation)) {
                    continue;
                }
                relationNames.add(relation);
            } else if (printLine[i].contains(";")) {
                relation = printLine[i].replace(";", "");
                if(checkForCatalogStatement(relation)) {
                    continue;
                }
                relationNames.add(relation);
            } else {
                relation = printLine[i];
                if(checkForCatalogStatement(relation)) {
                    continue;
                }
                relationNames.add(relation);
            }
        }
        return relationNames;
    }

    private static boolean checkForCatalogStatement(String relation) {
        if(relation.toUpperCase().equals("CATALOG")) {
            SurlyDatabase.hasCatalogStatement = true;
            return true;
        }
        return false;
    }
}
